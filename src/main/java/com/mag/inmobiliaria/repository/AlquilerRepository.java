package com.mag.inmobiliaria.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.mag.inmobiliaria.entity.Alquiler;
import com.mag.inmobiliaria.entity.Persona;

public class AlquilerRepository {

	public Alquiler guardarAlquiler(Alquiler alquiler) {
		// aca guardaria el alquiler
		return alquiler;
	}

	public double calcularPrecioFinal(Alquiler alquiler) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Calendar desde = Calendar.getInstance();
		Calendar hasta = Calendar.getInstance();
		desde.setTime(simpleDateFormat.parse(alquiler.getFechaInicio()));
		hasta.setTime(simpleDateFormat.parse(alquiler.getFechaFin()));

		int dias, meses, años;
		// calculo la diferencia de dias entre las dos fechas
		// le resto 1 para tener en cuenta el día en que se alquila
		dias = (hasta.get(Calendar.DAY_OF_MONTH)) - ((desde.get(Calendar.DAY_OF_MONTH)) - 1);

		// si da negativo quiere decir que no se cumplio un mes, pero si justo ese dia
		// es el ultimo dia del mes
		// si se cumple
		// ej: 31 de enero y termina el 28 de febrero, eso se cuenta como 1 mes
		if (0 > dias && hasta.getActualMaximum(Calendar.DAY_OF_MONTH) != hasta.get(Calendar.DAY_OF_MONTH)) {
			meses = (hasta.get(Calendar.MONTH)) - (desde.get(Calendar.MONTH)) - 1;
			dias = dias * -1;
		} else {
			meses = (hasta.get(Calendar.MONTH)) - (desde.get(Calendar.MONTH));
		}

		// si los dias totales son igual al maximo numero del maximo de dias de la fecha
		// fin, se incrementan los meses
		if (dias == hasta.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			meses += 1;
			dias = 0;
		}

		// si los meses son negativos resto uno al año para poner la verdadera
		// diferencia
		if (0 > meses) {
			años = (hasta.get(Calendar.YEAR)) - (desde.get(Calendar.YEAR)) - 1;
			// si ese caso pasa debo calcular el total de meses transcurridos
			meses += 12 - (hasta.get(Calendar.MONTH));
		} else {
			años = (hasta.get(Calendar.YEAR)) - (desde.get(Calendar.YEAR));
		}

		if (meses == 12) {
			años += 1;
			meses = 0;
		}

		int diasTotales = (int) ((hasta.getTimeInMillis() - desde.getTimeInMillis()) / (1000 * 60 * 60 * 24));

		double total = ((años * alquiler.getCasa().getPrecioMensual() * 12)
				+ (meses * alquiler.getCasa().getPrecioMensual()) + (dias * alquiler.getCasa().getPrecioPorNoche()));

		return total * calcularBonificaciones(diasTotales, alquiler.getPersona());

	}

	private double calcularBonificaciones(int diasTotales, Persona persona) {
		double descuentoDeDias = 0;
		if (diasTotales > 15) {
			descuentoDeDias = 0.15;
		} else if (diasTotales > 4) {
			descuentoDeDias = 0.05;
		}

		return 100 - (persona.obtenerDescuento() + descuentoDeDias);
	}
}
