package com.mag.inmobiliaria.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.mag.inmobiliaria.entity.Alquiler;
import com.mag.inmobiliaria.entity.Cliente;
import com.mag.inmobiliaria.repository.AlquilerRepository;
import com.mag.inmobiliaria.util.ResponseWrapper;

@Path("/alquiler")
public class AlquilerRestController {
	
	private AlquilerRepository alquilerRepository;
	
	@GET
	@Path("/getAlquileres/{idUsuario}")
	@Produces("application/json")
	public Object saludar() {
		System.out.println("llegamos");
		Cliente c = new Cliente();
		c.setDni("123123");
		return c;
	}
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public ResponseWrapper alquilar(Alquiler alquiler) {
		ResponseWrapper responseWrapper;
		try {
			alquilerRepository = new AlquilerRepository();
			System.out.println("llegamos");
			responseWrapper = new ResponseWrapper(alquilerRepository.guardarAlquiler(alquiler));
		}catch (Exception ex) {
			responseWrapper = new ResponseWrapper(ex);
		}
		return responseWrapper;
	}
}
