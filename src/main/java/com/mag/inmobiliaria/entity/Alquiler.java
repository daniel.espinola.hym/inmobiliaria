package com.mag.inmobiliaria.entity;

import javax.persistence.Entity;

@Entity()
public class Alquiler {

	private String fechaInicio;
	private String fechaFin;

	// private Date fechaInicio;
	// private Date fechaFin;

	private Casa casa;
	private Persona persona;
	
	public Alquiler() {
		
	}
	
	public Alquiler(String fechaInicio, String fechaFin, Casa casa, Persona persona) {

	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public Casa getCasa() {
		return casa;
	}

	public void setCasa(Casa casa) {
		this.casa = casa;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
