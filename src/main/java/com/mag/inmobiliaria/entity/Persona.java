package com.mag.inmobiliaria.entity;

public abstract class Persona {
	private String dni;
	private String nombre;
	private String apellido;
	private int edad;
	private Alquiler casaAlquilada;

	public abstract double obtenerDescuento();

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public Alquiler getCasaAlquilada() {
		return casaAlquilada;
	}

	public void setCasaAlquilada(Alquiler casaAlquilada) {
		this.casaAlquilada = casaAlquilada;
	}

}
