package com.mag.inmobiliaria.entity;

public class Casa {
	private String direccion;
	private int cantidadDeAmbientes;
	private float mtsCuadrados;
	private double precioPorNoche;
	private double precioMensual;

	public Casa() {

	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getCantidadDeAmbientes() {
		return cantidadDeAmbientes;
	}

	public void setCantidadDeAmbientes(int cantidadDeAmbientes) {
		this.cantidadDeAmbientes = cantidadDeAmbientes;
	}

	public float getMtsCuadrados() {
		return mtsCuadrados;
	}

	public void setMtsCuadrados(float mtsCuadrados) {
		this.mtsCuadrados = mtsCuadrados;
	}

	public double getPrecioPorNoche() {
		return precioPorNoche;
	}

	public void setPrecioPorNoche(double precioPorNoche) {
		this.precioPorNoche = precioPorNoche;
	}

	public double getPrecioMensual() {
		return precioMensual;
	}

	public void setPrecioMensual(double precioMensual) {
		this.precioMensual = precioMensual;
	}

}
