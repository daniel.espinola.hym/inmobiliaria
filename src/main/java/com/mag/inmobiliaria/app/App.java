package com.mag.inmobiliaria.app;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class App {

	public static void main(String[] args) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		sdf.setLenient(false);
		Calendar desde=Calendar.getInstance();
        Calendar hasta=Calendar.getInstance();
        desde.setTime(sdf.parse("31012018"));
        hasta.setTime(sdf.parse("01012019"));

		int dias,meses,años;
		//calculo la diferencia de dias entre las dos fechas
		//le resto 1 para tener en cuenta el día en que se alquila
		dias = (hasta.get(Calendar.DAY_OF_MONTH)) - ((desde.get(Calendar.DAY_OF_MONTH)) -1);
		
		//si da negativo quiere decir que no se cumplio un mes, pero si justo ese dia es el ultimo dia del mes
		//si se cumple
		//ej: 31 de enero y termina el 28 de febrero, eso se cuenta como 1 mes
		if(0>dias && hasta.getActualMaximum(Calendar.DAY_OF_MONTH) != hasta.get(Calendar.DAY_OF_MONTH)) {
			meses = (hasta.get(Calendar.MONTH)) - (desde.get(Calendar.MONTH)) - 1;
			dias=dias*-1;
		}else {
			meses = (hasta.get(Calendar.MONTH)) - (desde.get(Calendar.MONTH));
		}
		
		//si los dias totales son igual al maximo numero del maximo de dias de la fecha fin, se incrementan los meses
		if(dias == hasta.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			meses+=1;
			dias=0;
		}
		
		//si los meses son negativos resto uno al año para poner la verdadera diferencia
		if(0>meses) {
			años = (hasta.get(Calendar.YEAR)) - (desde.get(Calendar.YEAR)) - 1;
			//si ese caso pasa debo calcular el total de meses transcurridos
			meses+=12-(hasta.get(Calendar.MONTH));
		}else {
			años = (hasta.get(Calendar.YEAR)) - (desde.get(Calendar.YEAR));
		}
		
		if(meses == 12) {
			años+=1;
			meses=0;
		}
		
		System.out.println(hasta.get(Calendar.DAY_OF_MONTH));
		System.out.println(hasta.getActualMaximum(Calendar.DAY_OF_MONTH));
        System.out.println(dias+"---"+meses+"---"+años);
	}

}
