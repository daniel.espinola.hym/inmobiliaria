package com.mag.inmobiliaria.util;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.mag.inmobiliaria.controller.AlquilerRestController;

@ApplicationPath("/rest")
public class RestEasyServices extends Application {
 
    private Set<Object> singletons = new HashSet<Object>();
 
    public RestEasyServices() {
        singletons.add(new AlquilerRestController());
    }
 
    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}